import express from "../helpers/express";
const routes = express.Router();

import main from "./main";
import product from "./product";
import category from "./category";
import order from "./order";

routes.use("/", main);
routes.use("/product", product);
routes.use("/category", category);
routes.use("/order", order);

export default routes;
