import express from "../helpers/express";
import product from "../controllers/product";
const route = express.Router();

route.get("/", product.getAllProducts);
route.get("/category", product.getProductsByCategory);
route.get("/name", product.getProductsByName);
route.get("/sku", product.getProductBySku);
route.post("/", product.createProduct);
route.put("/", product.updateProduct);

export default route;
