import express from "../helpers/express";
const route = express.Router();
import category from "../controllers/category";

route.get("/", category.getCategoryById);
route.get("/all", category.getAllCategories);
route.post("/", category.createCategory);
route.put("/", category.updateCategory);

export default route;
