import express from "../helpers/express";
const route = express.Router();
import payu from "../controllers/payu";

route.get("/", (req, res) => {
  res.status(200).json({
    message: "Welcome",
  });
});

route.post("/payu", payu.receivePayment);

export default route;
