import express from "../helpers/express";
import order from "../controllers/order";
const route = express.Router();

route.post("/create", order.createOrder);
route.patch("/register", order.registerPayment);

export default route;
