import { productInCart } from "./types";

const formatNumber = (number: number) => {
  return number > 0 ? `$${new Intl.NumberFormat("de-DE").format(number)}` : "";
};

const sumQuantity = (cart: productInCart[]) => {
  return cart.reduce((total, { quantity, price }) => total + quantity, 0);
};

const sumAmount = (cart: productInCart[]) => {
  return formatNumber(
    cart.reduce(
      (total, { quantity, price, discount }) =>
        total + sumAmountProduct({ quantity, price, discount }),
      0
    )
  );
};

const sumAmountProduct = ({
  price,
  quantity,
  discount,
}: {
  price: number;
  quantity: number;
  discount: number | undefined;
}) => {
  if (discount && quantity > 2) {
    return price * ((100 - discount) / 100) * 2 + price * (quantity - 2);
  } else if (discount && quantity <= 2) {
    return price * ((100 - discount) / 100) * quantity;
  }

  return price * quantity;
};

export { formatNumber, sumQuantity, sumAmount, sumAmountProduct };
