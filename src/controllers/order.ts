import fs = require("fs");
import nodemailer = require("nodemailer");
import md5 = require("md5");
import Customer from "../models/Customer";
import Order from "../models/Order";
import handlebars from "../helpers/handlebars";

const createOrder = async (req: any, res: any, next: any) => {
  const { order } = req.body;
  const { customer, ...rest } = order;

  try {
    let newCustomer: any;
    const customerId = md5(`${customer.phone}~${customer.email}`);

    const currentCustomer = await Customer.findOne({
      id: customerId,
    });

    if (!currentCustomer) {
      newCustomer = await Customer.create({
        id: customerId,
        ...customer,
      });
    }

    const response = await Order.create({
      ...rest,
      customer: currentCustomer ? currentCustomer._id : newCustomer._id,
    });

    res.status(200).json({ message: "Order was created", response });
  } catch (error) {
    next(error);
  }
};

const registerPayment = async (req: any, res: any, next: any) => {
  const { payment } = req.body;
  const { referenceCode, method, ...details } = payment;
  const { SHIPPING_ICON, BRAND_ICON } = process.env;

  try {
    const response = await Order.findOneAndUpdate(
      { id: referenceCode },
      {
        payment: {
          method,
          details,
        },
      },
      { new: true }
    ).populate("customer");

    const {
      products,
      discountUnits,
      shipping,
      customer: {
        name,
        phone,
        email,
        city,
        address,
        addressDetail,
        neighborhood,
      },
    } = response;

    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      auth: {
        user: "juanromerol0509@gmail.com",
        pass: "iqrjradcfkwnymhn",
      },
    });

    const object = {
      id: referenceCode,
      products,
      discountUnits,
      shipping,
      name,
      phone,
      email,
      city,
      address,
      addressDetail,
      neighborhood,
      method,
      brandIcon: BRAND_ICON,
      shippingIcon: SHIPPING_ICON,
    };

    const source = fs.readFileSync("./views/email.hbs", "utf8");
    const template = handlebars.compile(source);

    const message = {
      to: `Nodemailer <${email}>`,
      subject: `Detalles de tú pedido # ${referenceCode} | Fruver y Mercado El Líder`,
      html: template(object),
    };

    const sentEmail = await transporter.sendMail(message);
    res.status(200).json({ message: "Payment was registered", sentEmail });
  } catch (error) {
    next(error);
  }
};

export default { createOrder, registerPayment };
