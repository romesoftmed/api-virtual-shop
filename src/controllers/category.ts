import Category from "../models/Category";

const getAllCategories = async (req: any, res: any, next: any) => {
  try {
    const categories = await Category.find({});
    res.status(200).json(categories);
  } catch (error) {
    next(error);
  }
};

const getCategoryById = async (req: any, res: any, next: any) => {
  const { categoryId } = req.query;
  try {
    const category = await Category.findOne({ id: categoryId });
    res.status(200).json(category);
  } catch (error) {
    next(error);
  }
};

const createCategory = async (req: any, res: any, next: any) => {
  const { category } = req.body;
  try {
    const result = await Category.create(category);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

const updateCategory = async (req: any, res: any, next: any) => {
  const { category } = req.body;
  try {
    const result = await Category.findOneAndUpdate(
      { id: category.id },
      category,
      {
        new: true,
      }
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

export default {
  getAllCategories,
  getCategoryById,
  createCategory,
  updateCategory,
};
