import Product from "../models/Product";
import { cleanProduct } from "../helpers/functions";

const getAllProducts = async (req: any, res: any, next: any) => {
  try {
    const products = await Product.find({});
    res.status(200).json(products);
  } catch (error) {
    next(error);
  }
};

const getProductsByCategory = async (req: any, res: any, next: any) => {
  const { categoryId } = req.query;
  try {
    const products = await Product.find({ category: categoryId });
    res.status(200).json(products);
  } catch (error) {
    next(error);
  }
};

const getProductsByName = async (req: any, res: any, next: any) => {
  const { searchParam } = req.query;
  try {
    const product = await Product.find({ name: { $regex: searchParam } });
    res.status(200).json(product);
  } catch (error) {
    next(error);
  }
};

const getProductBySku = async (req: any, res: any, next: any) => {
  const { sku } = req.query;
  try {
    const product = await Product.findOne({ sku });
    res.status(200).json(product);
  } catch (error) {
    next(error);
  }
};

const createProduct = async (req: any, res: any, next: any) => {
  const { product } = req.body;
  const newProduct = cleanProduct(product);
  try {
    const result = await Product.create(newProduct);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

const updateProduct = async (req: any, res: any, next: any) => {
  const { sku } = req.query;
  const { product } = req.body;
  try {
    const result = await Product.findOneAndUpdate({ sku }, product, {
      new: true,
    });
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

export default {
  getAllProducts,
  getProductsByCategory,
  getProductsByName,
  getProductBySku,
  createProduct,
  updateProduct,
};
