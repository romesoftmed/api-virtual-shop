export interface category {
  id: number;
  name: string;
  icon: string;
}

export interface product {
  sku: number;
  name: string;
  size: number;
  unit: string;
  price: number;
  img: string;
  discount?: number;
  quantity?: number;
}

export interface productInCart {
  sku: number;
  name: string;
  size: number;
  price: number;
  unit: string;
  quantity: number;
  discount?: number;
  pricePerUnit: string;
  img: string;
}
