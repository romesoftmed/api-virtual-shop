require("dotenv").config();
if (process.env.NODE_ENV === "production") {
  require("newrelic");
}

import http = require("http");
import express from "./helpers/express";
import routes from "./routes/router";
import cors = require("cors");
import mongoose = require("mongoose");
import bodyParser = require("body-parser");

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use("/", routes);

const server = http.createServer(app);
const { MONGO_SERVER, PORT } = process.env;
const port = PORT || 4000;

mongoose.connect(MONGO_SERVER, (err) => {
  if (err) {
    console.log(err);
  } else {
    server.listen(port, () => {
      console.log(`Listening on port: ${port}`);
    });
  }
});
