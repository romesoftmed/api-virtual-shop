export interface productInCart {
  sku: string;
  name: string;
  size: number;
  unit: string;
  price: number;
  img: string;
  quantity: number;
  discount?: number;
}
