export const cleanProduct = (product: any) => {
  const newProduct = {
    sku: product.sku,
    name: product.name,
    size: parseInt(product.size),
    unit: product.unit,
    cost: parseInt(product.cost),
    price: parseInt(product.price),
    img: product.img,
    discount: parseInt(product.discount),
    stock: parseInt(product.stock),
    category: product.category,
  };

  return newProduct;
};
