import { productInCart } from "./types";

const formatNumber = (number: number) => {
  return number > 0
    ? `$${new Intl.NumberFormat("de-DE", { maximumFractionDigits: 2 }).format(
        number
      )}`
    : "";
};

const getAmount = (cart: productInCart[]) => {
  return cart.reduce(
    (total, { quantity, price, discount }) =>
      total + getAmountProduct({ quantity, price, discount }),
    0
  );
};

const getAmountProduct = ({ price, quantity }: any) => {
  return price * quantity;
};

const getSaving = (cart: productInCart[]) => {
  return cart.reduce(
    (total, { quantity, price, discount }) =>
      total + getSavingProduct({ price, quantity, discount: discount || 0 }),
    0
  );
};

const getSavingProduct = ({
  price,
  quantity,
  discount,
  discountUnits = 2,
}: any) => {
  return (
    price *
    (discount / 100) *
    (quantity > discountUnits ? discountUnits : quantity)
  );
};

const discountedPrice = ({ price, discount }) => {
  return formatNumber(price * ((100 - discount) / 100));
};

const add = (a: number, b: number) => {
  return a + b;
};

const subs = (a: number, b: number) => {
  return a - b;
};

const gt = (a: number, b: number) => {
  return a > b;
};

export default {
  formatNumber,
  getAmount,
  getSaving,
  getAmountProduct,
  getSavingProduct,
  discountedPrice,
  add,
  subs,
  gt,
};
