import numbers from "../helpers/numbers";
import handlebars = require("handlebars");

handlebars.registerHelper("formatNumber", numbers.formatNumber);
handlebars.registerHelper("getAmount", numbers.getAmount);
handlebars.registerHelper("getSaving", numbers.getSaving);
handlebars.registerHelper("getAmountProduct", numbers.getAmountProduct);
handlebars.registerHelper("getSavingProduct", numbers.getSavingProduct);
handlebars.registerHelper("discountedPrice", numbers.discountedPrice);
handlebars.registerHelper("add", numbers.add);
handlebars.registerHelper("subs", numbers.subs);
handlebars.registerHelper("gt", numbers.gt);

export default handlebars;
