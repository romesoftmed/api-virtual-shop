import { Schema, model } from "mongoose";

const schemaOptions = {
  toJSON: {
    virtuals: true,
  },
};

const paymentSchema = new Schema(
  {
    method: { type: String, required: true },
    details: { type: Object, required: true },
  },
  schemaOptions
);

export default model("payment", paymentSchema);
