import { Schema, model } from "mongoose";

const schemaOptions = {
  toJSON: {
    virtuals: false,
  },
};

const orderSchema = new Schema(
  {
    id: { type: String, required: true, unique: true },
    products: { type: Array, required: true },
    discountUnits: { type: Number, required: true },
    shipping: { type: Number, required: true },
    customer: { type: Schema.Types.ObjectId, ref: "customer", required: true },
    payment: { type: Object, required: false },
  },
  schemaOptions
);

export default model("order", orderSchema);
