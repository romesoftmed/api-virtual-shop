import { Schema, model } from "mongoose";

const schemaOptions = {
  toJSON: {
    virtuals: false,
  },
};

const customerSchema = new Schema(
  {
    id: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    phone: { type: String, required: true },
    email: { type: String, required: true },
    city: { type: String, required: true },
    address: { type: String, required: true },
    addressDetail: { type: String, required: false },
    neighborhood: { type: String, required: false },
  },
  schemaOptions
);

export default model("customer", customerSchema);
