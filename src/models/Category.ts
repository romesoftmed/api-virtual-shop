import { Schema, model } from "mongoose";

const schemaOptions = {
  toJSON: {
    virtuals: true,
  },
};

const categorySchema = new Schema(
  {
    id: { type: String, unique: true },
    name: { type: String, required: true },
    icon: { type: String, required: true },
  },
  schemaOptions
);

export default model("category", categorySchema);
