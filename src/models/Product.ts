import { Schema, model } from "mongoose";

const schemaOptions = {
  toJSON: {
    virtuals: true,
  },
};

const productSchema = new Schema(
  {
    sku: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    size: { type: Number, required: true },
    unit: { type: String, required: true },
    cost: { type: Number, required: true },
    price: { type: Number, required: true },
    img: { type: String, required: true },
    discount: { type: Number, required: true, default: 0 },
    stock: { type: Number, required: true, default: 0 },
    category: {
      type: String,
      ref: "category",
      required: true,
    },
  },
  schemaOptions
);

export default model("product", productSchema);
